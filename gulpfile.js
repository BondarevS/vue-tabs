const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const del = require('del');
const eslint = require('gulp-eslint');
const ttf2eot = require('gulp-ttf2eot');
const ttf2woff = require('gulp-ttf2woff');
const ttf2woff2 = require('gulp-ttf2woff2');
const ttf2svg = require('gulp-ttf-svg');
const iconfont = require('gulp-iconfont');
const iconfontCss = require('gulp-iconfont-css');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const sassLint = require('gulp-sass-lint');
const browserSync = require('browser-sync')
    .create();
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const babelify = require('babelify');
const mode = require('gulp-mode')({
    modes: ['production', 'development'],
    default: 'development',
    verbose: false
});
const print = require('gulp-print').default;
const cached = require('gulp-cached');
const dependents = require('gulp-dependents');
const babel       =   require( 'gulp-babel' );


const buildRoot = './build';
const assetsRoot = './assets';
const paths = {
    build: {
        js: `${buildRoot}/js/`,
        styles: `${buildRoot}/css/`,
        images: `${buildRoot}/images/`,
        fonts: `${buildRoot}/fonts/`
    },
    src: {
        js: `${assetsRoot}/js/*.js`,
        styles: `${assetsRoot}/scss/*.scss`,
        fonts: `${assetsRoot}/fonts/`,
        images: `${assetsRoot}/images/*.{jpg,png,svg,ico,gif}*`,
        icons: `${assetsRoot}/icons/*.svg`
    },
    watch: {
        js: `${assetsRoot}/js/**/*.js`,
        styles: `${assetsRoot}/scss/**/*.scss`,
        fonts: `${assetsRoot}/fonts/*.*`,
        images: `${assetsRoot}/images/*.{jpg,png,svg,ico,gif}*`,
        icons: `${assetsRoot}/icons/*.svg`
    }
};

// ===== Dev server =====
gulp.task('server', (done) => {
    browserSync.init({
        server: {
            baseDir: './',
            index: 'index.html'
        }
    });
    done();
});

gulp.task('reload', (done) => {
    browserSync.reload();
    done();
});


// ===== Linters =====
gulp.task('lint:styles', (done) => {
    gulp.src(paths.src.styles)
        .pipe(sassLint({
            options: {
                formatter: 'stylish',
                'merge-default-rules': false
            },
            files: {ignore: ['**/_icons.scss', '**/_mixin.scss']},
            rules: {
                'no-ids': 2,
                'no-duplicate-properties': 2,
                'no-empty-rulesets': 2
            }
        }))
        .pipe(sassLint.format())
        .pipe(mode.production(sassLint.failOnError()));
    done();
});

gulp.task('lint:js', (done) => {
    gulp.src(paths.src.js)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(mode.production(sassLint.failOnError()));
    done();
});

gulp.task('lint:all', gulp.series('lint:styles', 'lint:js'));


// ===== Styles =====
gulp.task('styles', (done) => {
    gulp.src(paths.src.styles)
        .pipe(cached('scsscache'))
        .pipe(dependents())
        .pipe(mode.development(sourcemaps.init()))
        .pipe(mode.development(sass()))
        .pipe(mode.production(sass({outputStyle: 'compressed'})))
        .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 10'))
        .pipe(mode.production(rename({suffix: '.min'})))
        .pipe(mode.development(sourcemaps.write()))
        .pipe(print(filepath => `css: ${filepath}`))
        .pipe(gulp.dest(paths.build.styles));
    done();
});

gulp.task('watch:styles', () => {
    gulp.watch(paths.watch.styles, gulp.series('lint:styles', 'styles', 'reload'));
});


// ===== Javascript =====
gulp.task('js', (done) => {
    gulp.src(paths.src.js)
        .pipe(mode.development(sourcemaps.init()))
        .pipe(babel({
          presets: ['@babel/env']
        }))
        .pipe(mode.development(sourcemaps.init()))
        .pipe(uglify())
        .pipe(mode.development(sourcemaps.write()))
        .pipe(print(filepath => `js: ${filepath}`))
        .pipe(gulp.dest(paths.build.js));
    done();
});

gulp.task('watch:js', () => {
    gulp.watch(paths.watch.js, gulp.series('js', 'reload'));
});


// ===== Tasks for images =====
gulp.task('images:optimize', (done) => {
    gulp.src(paths.src.images)
        .pipe(cached('imagecache'))
        .pipe(dependents())
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 3}),
            imagemin.svgo({
                plugins: [{removeViewBox: true}]
            })
        ]))
        .pipe(gulp.dest(paths.build.images));
    done();
});

gulp.task('watch:image', () => {
    gulp.watch(paths.watch.js, gulp.series('images:optimize'));
});


// ===== Tasks for FONTS =====
gulp.task('ttf2eot', (done) => {
    gulp.src(`${paths.src.fonts}*.ttf`)
        .pipe(ttf2eot())
        .pipe(gulp.dest(paths.build.fonts));
    done();
});

gulp.task('ttf2woff', (done) => {
    gulp.src(`${paths.src.fonts}*.ttf`)
        .pipe(ttf2woff())
        .pipe(gulp.dest(paths.build.fonts));
    done();
});

gulp.task('ttf2woff2', (done) => {
    gulp.src(`${paths.src.fonts}*.ttf`)
        .pipe(ttf2woff2())
        .pipe(gulp.dest(paths.build.fonts));
    done();
});

gulp.task('ttf2svg', (done) => {
    gulp.src(`${paths.src.fonts}*.ttf`)
        .pipe(ttf2svg())
        .pipe(gulp.dest(paths.build.fonts));
    done();
});

gulp.task('fonts:copy', (done) => {
    gulp.src(`${paths.src.fonts}*.*`)
        .pipe(gulp.dest(paths.build.fonts));
    done();
});

gulp.task('fonts', gulp.series('fonts:copy', gulp.parallel('ttf2woff', 'ttf2woff2', 'ttf2eot', 'ttf2svg')));

gulp.task('watch:fonts', () => {
    gulp.watch(paths.watch.fonts, gulp.series('fonts'));
});


// ===== Tasks for ICONS =====
const runTimestamp = Math.round(Date.now() / 1000);

gulp.task('iconfont', (done) => {
    gulp.src([paths.src.icons], {base: 'assets'})
        .pipe(iconfontCss({
            path: 'assets/scss/icons/templates/_icons.scss',
            targetPath: '../../assets/scss/icons/_icons.scss',
            fontPath: '../fonts/',
            fontName: 'icons',
            cacheBuster: runTimestamp
        }))
        .pipe(iconfont({
            fontName: 'icons',
            timestamp: runTimestamp,
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
            normalize: true,
            fontHeight: 1001
        }))
        .pipe(gulp.dest(paths.build.fonts));
    done();
});


// ===== Tasks for BUILD =====
gulp.task('build:dev', gulp.series('styles', 'js', gulp.parallel('watch:styles', 'watch:js')));


gulp.task('clean', () => {
    del.sync([buildRoot], {force: true});
});


gulp.task('build:prod', gulp.series('clean', 'fonts', 'images:optimize', 'lint:styles', 'styles', 'lint:js', 'js'));
