require([
    "tools/vue",
    "tabs",
    "tab"
], (Vue, Tabs, Tab) => {

    new Vue({
        el: '#app',
        component: {
            "tabs": Tabs,
            "tab": Tab
        }
    })
});
