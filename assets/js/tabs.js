/**
 * component that initialized tabs
 * include 2 files tab & tabs
 * @author Sergey Bondarev
 * @import {vue} from 'vue'
 */

define(['tools/vue'], Vue => {
	return Vue.component('tabs', {
		props: {
			hashChecking: {
				type: Boolean,
				default: false,
			},
		},
		data() {
			return { tabs: [] };
		},
		created() {
			this.tabs = this.$children;
		},
		ready() {
			if (this.hashChecking) this.checkHash();
		},
		methods: {
			selectTab(selectedTab) {
				this.tabs.forEach(tab => {
					tab.selected = tab.name === selectedTab.name;
				});
			},
			checkHash() {
				if (!window.location.hash) return;
				const matchesExist = this.tabs.some(
					tab => tab.name === window.location.hash.slice(1)
				);

				if (matchesExist) {
					this.tabs.forEach(tab => {
						tab.selected = tab.name === window.location.hash.slice(1);
					});
				}
			},
		},
		template: `
      <div class="tabset">
        <ul class="tabset__list">
          <li v-for="tab in tabs"
          		class="tabset__item">
            <a :href="tab.href"
            	 class="tabset__link"
          		 :class="{ 'active': tab.selected }"
               @click="selectTab(tab)">{{{ tab.title }}}</a>
          </li>
        </ul>
      </div>

      <div class="tabset__content">
        <slot></slot>
      </div>
    `,
	});
});
