/**
 * component that initialized tab
 * include 2 files tab & tabs
 * @author Sergey Bondarev
 * @import {vue} from 'vue'
 * @param {String} [title] - required - title of tab
 * @param {String} [name] - required - name of tab (this name becomes a hash)
 * @param {Boolean} [selected] sets the active tab after init
 * @param {Boolean} [mounted] sets the method for hiding tabs. true - v-if / false - v-show
 */
define(['tools/vue'], Vue => {
	return Vue.component('tab', {
		props: {
			title: {
				type: String,
				required: true,
			},
			name: {
				type: String,
				required: true,
			},
			selected: {
				type: Boolean,
				default: false,
			},
			mounted: {
				type: Boolean,
				default: false,
			},
		},
		computed: {
			href() {
				return `#${this.name.toLowerCase().replace(/ /g, '-')}`;
			},
		},
		template: `
        <template v-if="mounted">
            <div v-if="selected"
                 class="tabset__content-item">
              <slot></slot>
            </div>
        </template>
        <template v-else>
            <div v-show="selected"
                 class="tabset__content-item">
              <slot></slot>
           </div>
        </template>
    `,
	});
});
